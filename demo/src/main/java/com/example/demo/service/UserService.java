package com.example.demo.service;

import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import com.example.demo.model.User;
import java.util.List;
import java.util.Optional;

@Service
public class  UserService {
    @Autowired
    private UserRepository userDao;

    public List<User> getUsers(){
        return userDao.findAll();
    }

    public Optional<User> getUser(Long id){
        return userDao.findById(id);
    }

    public Optional<User> createUser(User user) {
        userDao.save(user);
        return getUser(user.getId());
    }

    public Optional<User> update(User user) {
        userDao.save(user);
        return getUser(user.getId());
    }

    public void deleteUser(Long userId) {
        userDao.deleteById(userId);
    }



}
